package com.vanwijk.rohan

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_article.*
import kotlinx.android.synthetic.main.layout_newsitem.*

class ArticleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)

        val title = intent.getStringExtra(MainActivity.ARTICLE_INTENT_TITLE)
        val image = intent.getStringExtra(MainActivity.ARTICLE_INTENT_IMAGE)
        val url = intent.getStringExtra(MainActivity.ARTICLE_INTENT_URL)
        val summary = intent.getStringExtra(MainActivity.ARTICLE_INTENT_SUMMARY)

        textView_article_Title.text = title
        textView_article_Summary.text = summary

        val options = RequestOptions.centerCropTransform()
        Glide.with(this).load(image).placeholder(R.drawable.ic_launcher_background).apply(options).into(imageView_article)

        btn_readmore.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            startActivity(intent)
        }
    }
}
