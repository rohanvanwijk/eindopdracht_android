package com.vanwijk.rohan

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class ArticleAdapter(val context: Context,
                     val articles: ArrayList<Article>,
                     val onArticleListener: OnArticleListener): RecyclerView.Adapter<ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.layout_newsitem, parent, false)
        return ArticleViewHolder(view)
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val article = articles[position]
        holder.title.text = article.Title
        holder.itemView.setOnClickListener {
            onArticleListener.onItemClick(holder.adapterPosition)
        }
        val options = RequestOptions.centerCropTransform()
        Glide.with(context).load(article.Image).placeholder(R.drawable.ic_launcher_background).apply(options).into(holder.image)
    }

}