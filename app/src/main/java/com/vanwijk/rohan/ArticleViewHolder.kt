package com.vanwijk.rohan

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.layout_newsitem.view.*

class ArticleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val title: TextView = itemView.textView_title
    val image: ImageView = itemView.imageView_img

}