package com.vanwijk.rohan

import java.util.*
import kotlin.collections.ArrayList

data class Article(val Id: Int,
                   val Feed: Int,
                   val Title: String,
                   val Summary: String,
                   val PublishDate: String,
                   val Image: String,
                   val Url: String,
                   val Related: List<String>,
                   val Categories: ArrayList<Categorie>,
                   val isLiked: Boolean)

