package com.vanwijk.rohan

import retrofit2.Call
import retrofit2.http.GET

open interface InhollandService {
    @GET("api/Articles")
    fun articles(): Call<List<Article>>

    @GET("api/Articles")
    fun results(): Call<Result>
}