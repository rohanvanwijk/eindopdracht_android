package com.vanwijk.rohan

interface OnArticleListener {
    fun onItemClick(position: Int)
}