package com.vanwijk.rohan

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.Serializable

class MainActivity : AppCompatActivity(), OnArticleListener {

    var ArticleList = ArrayList<Article>()

    companion object {
        const val ARTICLE_INTENT_TITLE = "article_title"
        const val ARTICLE_INTENT_IMAGE = "article_image"
        const val ARTICLE_INTENT_SUMMARY = "article_summary"
        const val ARTICLE_INTENT_URL = "article_url"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        LoadAPI()

    }

    fun SetupLayout(_list: ArrayList<Article>) {
        val verticalList = LinearLayoutManager(this)
        val recyList = findViewById<RecyclerView>(R.id.myRecyView)
        recyList.layoutManager = verticalList
        val articleAdapter = ArticleAdapter(this, _list, this)
        recyList.adapter = articleAdapter
    }

    fun LoadAPI() {
        var result = Result
        val retrofit = Retrofit.Builder()
            .baseUrl("http://inhollandbackend.azurewebsites.net/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val inhollandservice = retrofit.create(InhollandService::class.java)

        inhollandservice.results().enqueue(object : Callback<Result> {
            override fun onFailure(call: Call<Result>, t: Throwable) {
                Log.e("HTTP", "Could not fetch data", t)
            }

            override fun onResponse(call: Call<Result>, response: Response<Result>) {
                if(response.isSuccessful && response.body() != null) {
                    val articles = response.body()!!
                    ArticleList = articles.Results
                    SetupLayout(articles.Results)
                }
            }
        })
    }

    override fun onItemClick(position: Int) {
        val articleIntent = Intent(this, ArticleActivity::class.java)
        articleIntent.putExtra(ARTICLE_INTENT_TITLE, ArticleList[position].Title)
        articleIntent.putExtra(ARTICLE_INTENT_IMAGE, ArticleList[position].Image)
        articleIntent.putExtra(ARTICLE_INTENT_SUMMARY, ArticleList[position].Summary)
        articleIntent.putExtra(ARTICLE_INTENT_URL, ArticleList[position].Url)
        startActivity(articleIntent)
    }
}
